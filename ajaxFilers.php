<?php
/*
Plugin Name: ajaxFilters
Description: Plugin search and filtering events whith AJAX.
Version: 1.0
Author: Vitaliy
*/

/**
 * translation
 */
add_action( 'plugins_loaded', 'true_load_plugin_textdomain' );
function true_load_plugin_textdomain() {
	load_plugin_textdomain( 'vitaliy', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
}

add_filter( 'locale', 'true_localize_theme' );
function true_localize_theme( $locale ) {
	if ( isset( $_GET['lang'] ) ) {
		return esc_attr( $_GET['lang'] );
	}

	return $locale;
}

/**
 * add widget - "my_ajax_filter_widget"
 */
add_action( 'widgets_init', 'my_ajax_filter_register_widgets' );
function my_ajax_filter_register_widgets() {
	register_widget( 'my_ajax_filter_widget' );
}

class my_ajax_filter_widget extends WP_Widget {
	public function __construct() {
		$widget_ops = array(
			'classname'   => 'filter_widget_class',
			'description' => __( 'Widget filtering events whith ajax' ),
		);
		$this->WP_Widget( 'my_ajax_filter_widget', 'AjaxFilters', $widget_ops );
		add_action( 'wp_ajax_my_filter', array( $this, 'my_filter_callback' ) );
		add_action( 'wp_ajax_nopriv_my_filter', array( $this, 'my_filter_callback' ) );
	}

	public function form( $instance ) {
		$defaults = array(
			'title'      => __( 'Event Filter', 'vitaliy' ),
			'num_events' => '5',
		);

		$instance   = wp_parse_args( ( array ) $instance, $defaults );
		$title      = $instance['title'];
		$num_events = $instance['num_events'];
		?>
		<p><?php _e( 'Title for widget:', 'vitaliy' ); ?>
			<input id="<?php esc_attr( $this->get_field_id( 'title' ) ); ?>" class="widefat"
			       name="<?php echo $this->get_field_name( 'title' ); ?>"
			       type="text" value="<?php echo esc_attr( $title ); ?>"/></p>
		<p><?php _e( 'Count events:', 'vitaliy' ) ?>
			<input id="<?php esc_attr( $this->get_field_id( 'num_events' ) ); ?>" class="widefat"
			       name="<?php echo esc_attr( $this->get_field_name( 'num_events' ) ); ?>"
			       type="text" value="<?php echo esc_attr( $num_events ); ?>"/>
		</p>
		<?php
	}

	public function update( $new_instance, $old_instance ) {
		$instance               = $old_instance;
		$instance['title']      = sanitize_text_field( $new_instance['title'] );
		$instance['num_events'] = trim( strip_tags( $new_instance['num_events'] ) );

		return $instance;
	}

	public function widget( $args, $instance ) {
		echo $args['before_widget'];
		$title      = apply_filters( 'widget_title', $instance['title'] );
		$num_events = ( empty( $instance['num_events'] ) ) ? '1' : $instance['num_events'];

		if ( ! empty( $title ) ) {
			echo $args['before_title'] . esc_html_e( $title, 'vitaliy' ) . $args['after_title'];
		}

		// enqueue my script
		$this->myajax_filter();
		?>

		<?php//view search form ?>
		<form method="post" action="my_filter" id="my-form" name="my-form">
			<p><?php _e( 'Search title:', 'vitaliy' ); ?>
				<input id="search-title" class="widefat" name="filter_event" type="text"/>
			</p>
			<p><?php _e( 'Search date:', 'vitaliy' ); ?><br/>
				<input id="search-date" class="widefat" name="date_event" type="date"/>
			</p>
			<p>
				<input id="my-hidden-field" class="widefat" type="hidden"
				       name="num_events" value="<?php echo esc_attr( $num_events ); ?>"/>
			</p>
			<input id="filter-ajax" class="widefat" type="submit" value="<?php _e( 'Search', 'vitaliy' ); ?>"/></form>
		<?php
		echo $args['after_widget'];

	}

	public function my_filter_callback( $args ) {
		$date_event   = ( empty( $_POST['date_event'] ) ) ? '' : trim( strip_tags( $_POST['date_event'] ) );
		$filter_event = ( empty( $_POST['filter_event'] ) ) ? '' : sanitize_text_field( $_POST['filter_event'] );
		$num_events   = ( empty( $_POST['num_events'] ) ) ? '1' : trim( strip_tags( $_POST['num_events'] ) );

		$args_query = array(
			'post_type'      => array( 'post', 'events', 'meets', 'mystores' ),
			'posts_per_page' => $num_events,
			'post_title'     => $filter_event,
			'date_query'     => array(
				array(
					'after' => $date_event,
				),
				'inclusive' => true,
			),
			'orderby'        => 'date',
			'order'          => 'ASC',
		);

		// filter for query
		add_filter( 'posts_where', 'title_filter', 10, 2 );
		function title_filter( $where, &$wp_query ) {
			global $wpdb;
			if ( $filter_event = $wp_query->get( 'post_title' ) ) {
				$where .= ' AND ' . $wpdb->posts . '.post_title LIKE \'%' . esc_sql( $wpdb->esc_like( $filter_event ) ) . '%\'';
			}

			return $where;
		}

		$new_filter_events = new WP_Query( $args_query );
		remove_filter( 'posts_where', 'title_filter', 10, 2 );
		if ( $new_filter_events->have_posts() ):
			while ( $new_filter_events->have_posts() ): $new_filter_events->the_post();
				?>
				<p>
					<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
						<?php the_title(); ?>
					</a>
				</p>
				<p><?php echo get_the_date( 'd-m-Y', get_the_ID() ); ?></p>
				<hr/>
				<?php
			endwhile;
		else: ?>
			<p><?php _e( 'Not Found', 'vitaliy' ); ?></p>
			<?php
		endif;
		wp_reset_postdata();
		wp_die();
	}

	public function myajax_filter() {
		wp_enqueue_script( 'custom-script', plugins_url( '/includes/js/my-custom.js?458', __FILE__ ), array( 'jquery' ) );
		wp_localize_script( 'custom-script', 'myajax',
			array(
				'url' => admin_url( 'admin-ajax.php' ),
			)
		);
	}

	public function title_filter( $where, &$wp_query ) {
		global $wpdb;
		if ( $filter_event = $wp_query->get( 'name' ) ) {
			$where .= ' AND ' . $wpdb->posts . '.name LIKE \'%' . esc_sql( $wpdb->esc_like( $filter_event ) ) . '%\'';
		}

		return $where;
	}
}