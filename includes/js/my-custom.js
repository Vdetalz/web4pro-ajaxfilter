/**
 * Created by User on 21.06.2017.
 */
jQuery(document).ready(function ($) {
    jQuery('#my-form').submit(function (event) {
        jQuery('.my_new_events').remove();

        var data = jQuery('#my-form').serialize();
        jQuery.post({
            url: myajax.url,
            data: data + '&action=my_filter',
            success: function (data) {
                if (data.length > 0 && data != '') {
                    jQuery('#filter-ajax').after(function () {
                        return "<div class='my_new_events'>" + data + "</div>";
                    });
                }
            }
        });
        return false;
    })
});
